﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RollerController : MonoBehaviour
{
    public float speed;
    public float jumpHeight;
    public Text countText;
    public Text winText;

    private int count;
    private Rigidbody rb;


    void Start() 
    {
        rb = GetComponent<Rigidbody>();
        count = 0;
        winText.text = "";
        SetCountText();
    }

    void FixedUpdate() 
    {
        float moveHorizontal = Input.GetAxis("Horizontal");
        float moveVertical = Input.GetAxis("Vertical");
        float moveUp = Input.GetAxis("Jump");

        Vector3 movement = new Vector3(moveHorizontal, 0.0f, moveVertical);

        if (rb.transform.position.y <= 0.5f) {
            movement.y = moveUp * jumpHeight;
        }

        rb.AddForce(movement * speed); 
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Pick Up")) 
        {
            other.gameObject.SetActive(false);
            count++;
            SetCountText();
        }
    }

    void SetCountText()
    {
        countText.text = "Count: " + count.ToString();
        if (count >= 12) 
        {
            winText.text = "You Win!";
        }
    }
}
