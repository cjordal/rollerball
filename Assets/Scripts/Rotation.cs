﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Rotation : MonoBehaviour
{
    public float rotate_x;
    public float rotate_y;
    public float rotate_z;
    
    private Vector3 obj_rotation;
    
    void Start()
    {
        obj_rotation = new Vector3(rotate_x, rotate_y, rotate_z);
    }

    void Update()
    {
        transform.Rotate(obj_rotation * Time.deltaTime);
    }
}
